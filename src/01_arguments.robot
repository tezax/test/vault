*** Settings ***
Library    OperatingSystem
Library    Process

*** Test Cases ***
01-01 No arguments
    Log    ${OUTPUT DIR}
    Given Vault command is without arguments
    When Vault command is executed
    Then Vault command exit code is not    0
    And Help message is present in stderr

*** Keywords ***
Vault command arguments are
    [Arguments]    ${arguments}=None
    Set Test Variable    ${test_vault_arguments}    ${arguments}

Vault command is without arguments
    Set Test Variable    ${test_vault_arguments}    ${EMPTY}

Vault command is executed
    [Arguments]    ${arguments}=${test_vault_arguments}
    ${result} =    Run Process    ./vault/vault.sh ${arguments}    shell=True
    Set Test Variable    ${test_vault_exit_code}    ${result.rc}
    Set Test Variable    ${test_vault_stdout}    ${result.stdout}
    Set Test Variable    ${test_vault_stderr}    ${result.stderr}

Vault command exit code is
    [Arguments]    ${expected_exit_code}    ${exit_code}=${test_vault_exit_code}
    Should Be Equal As Integers    ${exit_code}    ${expected_exit_code}

Vault command exit code is not
    [Arguments]    ${expected_exit_code}    ${exit_code}=${test_vault_exit_code}
    Should Not Be Equal As Integers    ${exit_code}    ${expected_exit_code}

Help message is present in stderr
    [Arguments]    ${stderr}=${test_vault_stderr}
    ${expected_help_message} =    Get File    help.template
    Should End With    ${stderr}    ${expected_help_message}
