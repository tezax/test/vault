#!/bin/sh -e

TEST_SRC_DIR="$(echo "$0" | sed -E 's#/[^/]+$##g')/src"
TEST_RESULTS_DIR="$(echo "$0" | sed -E 's#/[^/]+$##g')/test_result"
mkdir -p "${TEST_RESULTS_DIR}"
rm -rf "${TEST_RESULTS_DIR}/*"
docker build --no-cache \
    --build-arg LOGLEVEL=TRACE \
    -t vault-test-result \
    -f "${TEST_SRC_DIR}/Dockerfile" \
    .
sync
docker run --name vault-test-result --rm \
    -v "$(cd "${TEST_RESULTS_DIR}" && pwd)":/workspace \
    -e UID=$(id -u) \
    -e GID=$(id -g) \
    vault-test-result \
    /bin/sh -c 'cp -r . /workspace/; chown -R ${UID}:${GID} /workspace/; sync'
sync
